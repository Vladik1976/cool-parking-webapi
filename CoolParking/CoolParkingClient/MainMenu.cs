﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Json;

namespace CoolParkingClient
{
    class MainMenu
    {
        private static MainMenu instance = new MainMenu();
        private readonly HttpClient _client;
        private readonly string _endPointURL;
        public MainMenu()
        {
            _client = new HttpClient();
            _endPointURL = ConfigurationManager.AppSettings["EndPointURL"];


        }

        public void BasicMenu()
        {
            Console.WriteLine();
            Console.WriteLine("0 - Вивести на екран поточний баланс Паркінгу.");
            Console.WriteLine("1 - Вивести на екран суму зароблених коштів за поточний період (до запису у лог).");
            Console.WriteLine("2 - Вивести на екран кількість вільних місць на паркуванні (вільно X з Y).");
            Console.WriteLine("3 - Вивести на екран усі Транзакції Паркінгу за поточний період (до запису у лог)");
            Console.WriteLine("4 - Вивести на екран історію Транзакцій (зчитавши дані з файлу Transactions.log)");
            Console.WriteLine("5 - Вивести на екран список Тр. засобів , що знаходяться на Паркінгу");
            Console.WriteLine("6 - Поставити Транспортний засіб на Паркінг");
            Console.WriteLine("7 - Забрати Транспортний засіб з Паркінгу");
            Console.WriteLine("8 - Поповнити баланс конкретного Тр. засобу");
        }
        public async Task AddBalance()
        {
            Console.WriteLine("Введіть реєстраційний номер тр. засобу");
            string idcar = Console.ReadLine();
            Console.WriteLine("Введіть суму поповнення");
            decimal balance = Convert.ToInt32(Console.ReadLine());
            TransactionDTO t = new();
            t.Id = idcar;
            t.Sum = balance;
            HttpResponseMessage response = await _client.PutAsJsonAsync(_endPointURL + $"/api/transactions/topUpVehicle", t);

            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    {
                        Console.Write("Баланс успішно поповнено.");
                        break;
                    }
                case HttpStatusCode.NotFound:
                    {
                        Console.Write("Помилка. Даний автомобіль на паркінгу відсутній.");
                        break;
                    }
                default:
                    {
                        Console.Write("An error occured");
                        break;
                    }
            }







            try
            {
                ParkingService.Instance.TopUpVehicle(idcar, balance);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }

        }
        public async Task ShowListOfVehicles()
        {
            var _veh = await _client.GetStringAsync(_endPointURL + $"/api/vehicles");
            List<VehicleDTO> _vehicles = JsonConvert.DeserializeObject<List<VehicleDTO>>(_veh);
            foreach (VehicleDTO v in _vehicles)
                Console.WriteLine($"Vehicle ID: {v.Id}, Vehicle Type: {(VehicleType)v.VehicleType}, balance: {v.Balance}");

        }
        public async Task ShowTransactionsLog()
        {
            HttpResponseMessage response = await _client.GetAsync(_endPointURL + $"/api/transactions/all");
            switch (response.StatusCode)
            {
                case HttpStatusCode.OK:
                    {
                        Console.Write(response.Content.ReadAsStringAsync().Result);
                        break;
                    }
                case HttpStatusCode.NotFound:
                    {
                        Console.Write("Log file not found.");
                        break;
                    }
                default:
                    {
                        Console.Write("An error occured");
                        break;
                    }
            }
        }

        public async Task ShowTransactions()
        {
            var _transac = await _client.GetStringAsync(_endPointURL + $"/api/transactions/last");
            List<TransactionInfo>  _transactions= JsonConvert.DeserializeObject<List<TransactionInfo>>(_transac);

            foreach (TransactionInfo tranc in _transactions)
                Console.WriteLine($"Id {tranc.VehicleId}, date {tranc.TransactionDate}, paid: ${tranc.Sum} ");
        }

        public async Task CountFreeSpace()
        {
            var _freeplaces = await _client.GetStringAsync(_endPointURL + $"/api/parking/freePlaces");
            Console.WriteLine("На парковці вільно " + _freeplaces.ToString() + " місць");
        }

        public void IncorectInput()
        {
            Console.WriteLine("Incorect input");
        }
        public async Task AddNewcar()
        {
            SelectTypeCar();
            var input = Console.ReadLine();
            if (int.TryParse(input, out int a))
            {
                Console.WriteLine("Реєстраційний номер в форматі АА-0000-АА");
                var Reg = Console.ReadLine();
                Console.WriteLine("Початковий баланс");
                var Bal = Console.ReadLine();
                if (decimal.TryParse(Bal, out decimal b))
                {
                    int type = Convert.ToInt32(input);
                    decimal Balance = Convert.ToDecimal(Bal);
                    VehicleDTO v = new ();
                    v.Id = Reg;
                    v.VehicleType = type;
                    v.Balance = Balance;
                    HttpResponseMessage response = await _client.PostAsJsonAsync(_endPointURL + $"/api/vehicles", v);

                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.Created:
                            {
                                Console.Write("Автомобіль додано.");
                                break;
                            }
                        case HttpStatusCode.BadRequest:
                            {
                                Console.Write("Помилка. Перевірте дані та спробуйте ще.");
                                break;
                            }
                        default:
                            {
                                Console.Write("An error occured");
                                break;
                            }
                    }

                }
            }
        }

        public void SelectTypeCar()
        {
            Console.WriteLine("Тип транспортного засобу");
            Console.WriteLine("1 - Автомобіль");
            Console.WriteLine("2 - Вантажівка");
            Console.WriteLine("3 - Автобус");
            Console.WriteLine("4 - Мотоцикл\n");
            Console.WriteLine("й - Меню");
        }

        public async Task ShowParkingBalance()
        {
            var Balance = await _client.GetStringAsync(_endPointURL + $"/api/parking/balance");
            Console.WriteLine("Поточний баланс паркінгу " + Balance.ToString());
        }
        public async Task RemoveCar()
        {
            Console.WriteLine("Введіть реєстраційний номер");
            string id = Console.ReadLine();
            HttpResponseMessage _response = await _client.DeleteAsync(_endPointURL + $"/api/vehicles/{id}");
            switch (_response.StatusCode)
            {
                case HttpStatusCode.NoContent:
                    {
                        Console.Write("Автомобіль видалено.");
                        break;
                    }
                case HttpStatusCode.BadRequest:
                    {
                        Console.Write("Помилка. Перевірте дані та спробуйте ще.");
                        break;
                    }
                case HttpStatusCode.NotFound:
                    {
                        Console.Write("Помилка. Автомобіль не знайдено");
                        break;
                    }
                default:
                    {
                        Console.Write("An error occured");
                        break;
                    }
            }

        }

        public static MainMenu Instance { get { return instance; } }
    }
}
