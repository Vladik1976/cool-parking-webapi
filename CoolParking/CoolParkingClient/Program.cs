﻿using System;

namespace CoolParkingClient
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                MainMenu.Instance.BasicMenu();

                var input = Console.ReadKey();
                Console.WriteLine();
                switch (input.KeyChar)
                {
                    case '0':
                        MainMenu.Instance.ShowParkingBalance().Wait();
                        break;
                    case '1':
                        MainMenu.Instance.ShowParkingBalance().Wait();
                        break;
                    case '2':
                        MainMenu.Instance.CountFreeSpace().Wait();
                        break;
                    case '3':
                        MainMenu.Instance.ShowTransactions().Wait();
                        break;
                    case '4':
                        MainMenu.Instance.ShowTransactionsLog().Wait();
                        break;
                    case '5':
                        MainMenu.Instance.ShowListOfVehicles().Wait();
                        break;
                    case '6':
                        MainMenu.Instance.AddNewcar().Wait();
                        break;
                    case '7':
                        MainMenu.Instance.RemoveCar().Wait();
                        break;
                    case '8':
                        MainMenu.Instance.AddBalance().Wait();
                        break;
                    case 'q':
                        Console.Clear();
                        break;
                    default:
                        MainMenu.Instance.IncorectInput();
                        break;
                }

            }
        }
    }
}
