﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private ParkingService _parkingService;
        public VehiclesController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public List<Vehicle> GetAll()
        {
            return  _parkingService.GetVehicles().ToList();
        }
        [Route("{id}")]
        [HttpGet]
        public Vehicle Get(string id)
        {
            if (Vehicle.CheckVehicleID(id))
            {
                Vehicle _vehicle = _parkingService.GetVehicles().FirstOrDefault(Vehicle => Vehicle.Id == id);
                if (_vehicle == null)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                else
                {
                    return _vehicle;
                }
            }
            else
            {
                Response.StatusCode = 400;
                return null;
            }
        }
        [HttpDelete]
        [Route("{id}")]
        public string Delete(string id)
        {
            if (Vehicle.CheckVehicleID(id))
            {
                try
                {
                    _parkingService.RemoveVehicle(id);
                    Response.StatusCode = 204;
                    return null;
                }
                catch (ArgumentException e)
                {
                    Response.StatusCode = 404;
                    return e.Message;
                }
                catch ( InvalidOperationException e)
                {
                    Response.StatusCode = 400;
                    return e.Message;
                }
            }
            else
            {
                Response.StatusCode = 400;
                return null;
            }
        }
        [HttpPost]
        public Vehicle Add([FromBody] Vehicle vehicle)
        {
            

            try
            {
                Vehicle _vehicle = new(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                _parkingService.AddVehicle(_vehicle);
                Response.StatusCode = 201;
                return _vehicle;
            }
            catch
            {
                Response.StatusCode = 400;
                return null;
            }
        }
    }
}
