﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking/[action]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private ParkingService _parkingService;

        public ParkingController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet]
        public decimal Balance()
        {
            return _parkingService.GetBalance();
        }
        [HttpGet]
        public int Capacity()
        {
            return _parkingService.GetCapacity();
        }
        [HttpGet]
        public int FreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }


    }

}
