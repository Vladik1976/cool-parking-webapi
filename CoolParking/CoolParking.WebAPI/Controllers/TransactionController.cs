﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private ParkingService _parkingService;
        public TransactionController(ParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [Route("last")]
        [HttpGet]
        public List<TransactionInfo> GetLast()
        {
            return _parkingService.GetLastParkingTransactions().ToList();
        }
        [Route("all")]
        [HttpGet]
        public string GetAll()
        {
            try
            {
                return _parkingService.ReadFromLog();
            }
            catch(InvalidOperationException e)
            {
                Response.StatusCode = 404;
                return e.Message;
            }
        }
        [Route("topUpVehicle")]
        [HttpPut]
        public Vehicle TopUpVehicle([FromBody] TransactionDTO transaction)
        {
            if (transaction.Sum > 0 && Vehicle.CheckVehicleID(transaction.Id))
            {
                try
                {
                    _parkingService.TopUpVehicle(transaction.Id, transaction.Sum);
                    Vehicle _vehicle = _parkingService.GetVehicles().FirstOrDefault(Vehicle => Vehicle.Id == transaction.Id);
                    return _vehicle;
                }
                catch (ArgumentException e)
                {
                    Response.StatusCode = 404;
                    return null;
                }
                catch
                {
                    Response.StatusCode = 400;
                    return null;
                }
            }
            else
            {
                Response.StatusCode = 400;
                return null;
            }
        }
    }
}
