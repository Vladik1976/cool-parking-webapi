﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using Newtonsoft.Json;
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        private decimal _Sum;
        private DateTime _Date;
        private string _VehicleID;

        public TransactionInfo(string VehicleID, decimal Sum)
        {
            this._VehicleID = VehicleID;
            this._Date = DateTime.Now;
            this._Sum = Sum;
        }
        [JsonProperty("id")]
        public string VehicleId
        {
            get
            {
                return _VehicleID;
            }
            set
            {
                _VehicleID=value;
            }
        }
        [JsonProperty("sum")]
        public decimal Sum
        {
            get
            {
                return _Sum;
            }
            set
            {
                _Sum=value;
            }
        }
        public DateTime TransactionDate
        {
            get
            {
                return this._Date;
            }
        }

    }
}