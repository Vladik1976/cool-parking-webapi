﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        StreamWriter writer;
        FileStream file;
        public LogService(string FilePath)
        {
            LogPath = FilePath;
            
        }
        public string LogPath { get; set; }

        public string Read()
        {
            if (File.Exists(LogPath))
            {
                file = File.Open(LogPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                lock (file)
                {
                    file.Seek(0, SeekOrigin.Begin);
                    try
                    {
                        return new StreamReader(file).ReadToEnd();
                    }
                    finally
                    {
                        file.Seek(0, SeekOrigin.End);
                        file.Close();
                        file.Dispose();
                    }
                }
               
            }
            else
            {
                throw new InvalidOperationException("File does not exists!");
            }
        }

        public void Write(string logInfo)
        {
            file = File.Open(LogPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            file.Seek(0, SeekOrigin.End);
            writer = new StreamWriter(file)
            {
                AutoFlush = true
            };
            writer.Write(logInfo + "\r\n");
            writer.Close();
            writer.Dispose();
            file.Close();
            file.Dispose();
        }
    }
}